===========
parabolaiso
===========

The parabolaiso project features scripts and configuration templates to build installation media (*.iso* images) for BIOS
and UEFI based systems on the x86_64 and i686 architectures. It is based in `archiso <https://gitlab.archlinux.org/archlinux/archiso>`_
and `archiso32 <https://git.archlinux32.org/archiso32>`_.
Currently creating the images is only supported on Parabola GNU/Linux-libre.

Requirements
============

The following packages need to be installed to be able to create an image with the included scripts:

* arch-install-scripts
* awk
* dosfstools
* e2fsprogs
* erofs-utils (optional)
* findutils
* grub
* gzip
* libarchive
* libisoburn
* mtools
* openssl
* pacman
* sed
* squashfs-tools

For running the images in a virtualized test environment the following packages are required:

* edk2-ovmf
* qemu

For linting the shell scripts the following package is required:

* shellcheck

Profiles
========

parabolaiso comes with the following profiles: **baseline**, **releng**, **releng-openrc**, **lxde-openrc** and **talkingparabola**.
They can be found below `configs/baseline/ <configs/baseline/>`_, `configs/releng/ <configs/releng/>`_,
`configs/releng-openrc/ <configs/releng-openrc/>`_, `configs/lxde-openrc/ <configs/lxde-openrc/>`_, `configs/talkingparabola/ <configs/talkingparabola/>`_
(respectively). Profiles are defined by files to be placed into overlays (e.g. airootfs → the image's ``/``).

Read `README.profile.rst <docs/README.profile.rst>`_ to learn more about how to create profiles.

Create images
=============

Usually the parabolaiso tools are installed as a package. However, it is also possible to clone this repository and create
images without installing parabolaiso system-wide.

As filesystems are created and various mount actions have to be done when creating an image, **root** is required to run
the scripts.

When parabolaiso is installed system-wide and the modification of a profile is desired, it is necessary to copy it to a
writeable location, as ``/usr/share/parabolaiso`` is tracked by the package manager and only writeable by root (changes will
be lost on update).

The examples below will assume an unmodified profile in a system location (unless noted otherwise).

It is advised to consult the help output of **mkparabolaiso**:

.. code:: sh

   mkparabolaiso -h

Create images with packaged parabolaiso
---------------------------------------

.. code:: sh

  mkparabolaiso -w path/to/work_dir -o path/to/out_dir path/to/profile

Create images with local clone
------------------------------

Clone this repository and run:

.. code:: sh

  ./parabolaiso/mkparabolaiso -w path/to/work_dir -o path/to/out_dir path/to/profile

Testing
=======

The convenience script **run_parabolaiso** is provided to boot into the medium using qemu.
It is advised to read its help information:

.. code:: sh

  run_parabolaiso -h

Run the following to boot the iso using BIOS:

.. code:: sh

  run_parabolaiso -i path/to/a/parabola.iso

Run the following to boot the iso using UEFI:

.. code:: sh

  run_parabolaiso -u -i path/to/a/parabola.iso

The script can of course also be executed from this repository:


.. code:: sh

  ./scripts/run_parabolaiso.sh -i path/to/a/parabola.iso

Installation
============

To install parabolaiso system-wide use the included ``Makefile``:

.. code:: sh

  make install

Optional features

The iso image contains a GRUB environment block holding the iso name and version. This allows to
boot the iso image from GRUB with a version specific cow directory to mitigate overlay clashes.

.. code:: sh

  loopback loop parabola.iso
  load_env -f (loop)/boot/grub/grubenv
  linux (loop)/parabola/boot/x86_64/vmlinuz-linux-libre ... \
      cow_directory=parabola/${VERSION} ...
  initrd (loop)/parabola/boot/x86_64/initramfs-linux-libre-lts.img

Contribute
==========

Development of parabolaiso takes place on Parabola GNU/Linux-libre' Git: https://git.parabola.nu/parabolaiso.git.

Read our `contributing guide <CONTRIBUTING.rst>`_ to learn more about how to provide fixes or improvements for the code
base.

Discussion around parabolaiso takes place on the `dev mailing list
<https://lists.parabola.nu/listinfo/dev>`_ and in `#parabola
<ircs://irc.libera.chat/parabola>`_ on `freenode.net <ircs://irc.libera.chat/>`_.

All past and present authors of parabolaiso are listed in `AUTHORS <AUTHORS.rst>`_.

Releases
========

`Releases of parabolaiso <https://git.parabola.nu/parabolaiso.git/refs/tags>`_ are created by their current maintainers

- `David P <https://www.parabola.nu/people/hackers/#megver83>`_ (``6DB9C4B4F0D8C0DC432CF6E4227CA7C556B2BA78``)

Tags are signed using respective PGP keys.

To verify a tag, first import the relevant PGP key(s):

.. code:: sh

  gpg --auto-key-locate wkd --search-keys megver83@parabola.nu

or

.. code:: sh

  gpg --auto-key-locate keyserver --recv-keys 6DB9C4B4F0D8C0DC432CF6E4227CA7C556B2BA78

Afterwards a tag can be verified from a clone of this repository:

.. code:: sh

  git verify-tag <tag>

License
=======

parabolaiso is licensed under the terms of the **GPL-3.0-or-later** (see `LICENSE <LICENSE>`_).
